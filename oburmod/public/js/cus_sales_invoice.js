

frappe.ui.form.on('Sales Invoice', {

    refresh: function (frm) {
        frm.add_custom_button(__('End of Day'), 
        function() {
            erpnext.utils.map_current_doc({
                method: "oburmod.oburmod.doctype.end_of_day.end_of_day.make_sales_invoice",
                source_doctype: "End of Day",
                target: me.frm,
                setters: {
                    customer: me.frm.doc.customer || undefined,
                },
                get_query_filters: {
                    docstatus: 1,
                    // status: ["not in", ["Closed", "On Hold"]],
                    // per_billed: ["<", 99.99],
                    company: me.frm.doc.company
                }
            })
        }, __("Get Items From"));
    },
    income_account: function (frm) {
        for (let i in frm.doc.items) {
            frm.doc.items[i].income_account = frm.doc.income_account
          }
          frm.refresh_field('items')
    }

});


