

frappe.ui.form.on('Stock Entry', {
    refresh: function (frm) {
        frm.set_query('stock_entry_type', function () {
            return {
            //  'stock_entry_type': frm.doc.stock_entry_type
                query: "oburmod.queries.stock_entry_type_query",
                filters: {
                    // 'name': ['in', ['Repack']]
                }   
            }
        })
        for_stock_entry_type(frm)
        for_transfer_type(frm)
        // frm.add_custom_button(__('Hi'),
        //     function () {
        //         frappe.msgprint('Hi!')
        //     }, __("Say Hi"));
    },
    stock_entry_type: function (frm) {
        for_stock_entry_type(frm)
    },
    transfer_type: function (frm) {
        for_transfer_type(frm)
    },

});

function for_stock_entry_type(frm) {
    if (frm.doc.stock_entry_type === 'Material Transfer') {
        frm.set_df_property('transfer_type', 'hidden', 0)
    }
    else {
        frm.set_df_property('work_order_number', 'reqd', 0)
        frm.set_df_property('work_order_number', 'hidden', 1)
        frm.set_df_property('transfer_type', 'hidden', 1)
    }
}

function for_transfer_type(frm) {
    if (frm.doc.transfer_type === 'Damaged') {
        frm.set_df_property('work_order_number', 'reqd', 1)
        frm.set_df_property('work_order_number', 'hidden', 0)
    }
    else {
        frm.set_df_property('work_order_number', 'reqd', 0)
        frm.set_df_property('work_order_number', 'hidden', 1)
    }
}


