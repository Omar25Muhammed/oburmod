// Copyright (c) 2023, ARD and contributors
// For license information, please see license.txt

// {% include 'erpnext/selling/sales_common.js' %}

frappe.ui.form.on("End of Day", {
	refresh: function (frm) {
		
		frm.add_custom_button(__('Sales Invoice'), function () {
            //perform desired action such as routing to new form or fetching etc.
            // frappe.msgprint(__('Hi!'), 'Saying Hi')
			// frappe.new_doc('Sales Invoice')
			// frappe.call({
			// 	doc: frm.doc,
			// 	method: "make_sales_invoice",
			// 	args: {
			// 		// company: company,
			// 		// doc: frm.doc.name
			// 	},
			// 	callback: function (r) { }
			// })
			frappe.run_serially([
				() => frappe.new_doc('Sales Invoice'),
				() => {
					// var t = new Array
					// t.concat(frm.doc.items, frm.doc.items2)
					cur_frm.doc.customer = frm.doc.customer
					cur_frm.doc.due_date = frappe.datetime.nowdate()
					// cur_frm.doc.items = t
					// cur_frm.doc.items = frm.doc.items
					for (let i in frm.doc.items)
						{
							cur_frm.add_child('items', {
								"item_code": frm.doc.items[i]["item_code"],
								"customer_item_code": frm.doc.items[i]["customer_item_code"],
								"ensure_delivery_based_on_produced_serial_no": frm.doc.items[i]["ensure_delivery_based_on_produced_serial_no"],
								"delivery_date": frm.doc.items[i]["delivery_date"],
								"item_name": frm.doc.items[i]["item_name"],
								"description": frm.doc.items[i]["description"],
								"item_group": frm.doc.items[i]["item_group"],
								"brand": frm.doc.items[i]["brand"],
								"image_section": frm.doc.items[i]["image_section"],
								"image": frm.doc.items[i]["image"],
								"image_view": frm.doc.items[i]["image_view"],
								"quantity_and_rate": frm.doc.items[i]["quantity_and_rate"],
								"qty": frm.doc.items[i]["qty"],
								"stock_uom": frm.doc.items[i]["stock_uom"],
								"picked_qty": frm.doc.items[i]["picked_qty"],
								"uom": frm.doc.items[i]["uom"],
								"conversion_factor": frm.doc.items[i]["conversion_factor"],
								"stock_qty": frm.doc.items[i]["stock_qty"],
								"price_list_rate": frm.doc.items[i]["price_list_rate"],
								"base_price_list_rate": frm.doc.items[i]["base_price_list_rate"],
								"discount_and_margin": frm.doc.items[i]["discount_and_margin"],
								"margin_type": frm.doc.items[i]["margin_type"],
								"margin_rate_or_amount": frm.doc.items[i]["margin_rate_or_amount"],
								"rate_with_margin": frm.doc.items[i]["rate_with_margin"],
								"discount_percentage": frm.doc.items[i]["discount_percentage"],
								"discount_amount": frm.doc.items[i]["discount_amount"],
								"base_rate_with_margin": frm.doc.items[i]["base_rate_with_margin"],
								"rate": frm.doc.items[i]["rate"],
								"amount": frm.doc.items[i]["amount"],
								"item_tax_template": frm.doc.items[i]["item_tax_template"],
								"base_rate": frm.doc.items[i]["base_rate"],
								"base_amount": frm.doc.items[i]["base_amount"],
								"pricing_rules": frm.doc.items[i]["pricing_rules"],
								"stock_uom_rate": frm.doc.items[i]["stock_uom_rate"],
								"is_free_item": frm.doc.items[i]["is_free_item"],
								"grant_commission": frm.doc.items[i]["grant_commission"],
								"net_rate": frm.doc.items[i]["net_rate"],
								"net_amount": frm.doc.items[i]["net_amount"],
								"base_net_rate": frm.doc.items[i]["base_net_rate"],
								"base_net_amount": frm.doc.items[i]["base_net_amount"],
								"billed_amt": frm.doc.items[i]["billed_amt"],
								"valuation_rate": frm.doc.items[i]["valuation_rate"],
								"gross_profit": frm.doc.items[i]["gross_profit"],
								"drop_ship_section": frm.doc.items[i]["drop_ship_section"],
								"delivered_by_supplier": frm.doc.items[i]["delivered_by_supplier"],
								"supplier": frm.doc.items[i]["supplier"],
								"item_weight_details": frm.doc.items[i]["item_weight_details"],
								"weight_per_unit": frm.doc.items[i]["weight_per_unit"],
								"total_weight": frm.doc.items[i]["total_weight"],
								"weight_uom": frm.doc.items[i]["weight_uom"],
								"warehouse_and_reference": frm.doc.items[i]["warehouse_and_reference"],
								"warehouse": frm.doc.items[i]["warehouse"],
								"target_warehouse": frm.doc.items[i]["target_warehouse"],
								"prevdoc_docname": frm.doc.items[i]["prevdoc_docname"],
								"against_blanket_order": frm.doc.items[i]["against_blanket_order"],
								"blanket_order": frm.doc.items[i]["blanket_order"],
								"blanket_order_rate": frm.doc.items[i]["blanket_order_rate"],
								"manufacturing_section_section": frm.doc.items[i]["manufacturing_section_section"],
								"bom_no": frm.doc.items[i]["bom_no"],
								"planning_section": frm.doc.items[i]["planning_section"],
								"projected_qty": frm.doc.items[i]["projected_qty"],
								"actual_qty": frm.doc.items[i]["actual_qty"],
								"ordered_qty": frm.doc.items[i]["ordered_qty"],
								"planned_qty": frm.doc.items[i]["planned_qty"],
								"work_order_qty": frm.doc.items[i]["work_order_qty"],
								"produced_qty": frm.doc.items[i]["produced_qty"],
								"delivered_qty": frm.doc.items[i]["delivered_qty"],
								"returned_qty": frm.doc.items[i]["returned_qty"],
								"shopping_cart_section": frm.doc.items[i]["shopping_cart_section"],
								"additional_notes": frm.doc.items[i]["additional_notes"],
								"page_break": frm.doc.items[i]["page_break"],
								"item_tax_rate": frm.doc.items[i]["item_tax_rate"],
								"transaction_date": frm.doc.items[i]["transaction_date"]
							})
					}
					for (let i in frm.doc.items2)
					{
						cur_frm.add_child('items', {
							// "income_account": 
							"item_code": frm.doc.items2[i]["item_code"],
							"customer_item_code": frm.doc.items2[i]["customer_item_code"],
							"ensure_delivery_based_on_produced_serial_no": frm.doc.items2[i]["ensure_delivery_based_on_produced_serial_no"],
							"delivery_date": frm.doc.items2[i]["delivery_date"],
							"item_name": frm.doc.items2[i]["item_name"],
							"description": frm.doc.items2[i]["description"],
							"item_group": frm.doc.items2[i]["item_group"],
							"brand": frm.doc.items2[i]["brand"],
							"image_section": frm.doc.items2[i]["image_section"],
							"image": frm.doc.items2[i]["image"],
							"image_view": frm.doc.items2[i]["image_view"],
							"quantity_and_rate": frm.doc.items2[i]["quantity_and_rate"],
							"qty": -frm.doc.items2[i]["qty"],
							"stock_uom": frm.doc.items2[i]["stock_uom"],
							"picked_qty": frm.doc.items2[i]["picked_qty"],
							"uom": frm.doc.items2[i]["uom"],
							"conversion_factor": frm.doc.items2[i]["conversion_factor"],
							"stock_qty": frm.doc.items2[i]["stock_qty"],
							"price_list_rate": frm.doc.items2[i]["price_list_rate"],
							"base_price_list_rate": frm.doc.items2[i]["base_price_list_rate"],
							"discount_and_margin": frm.doc.items2[i]["discount_and_margin"],
							"margin_type": frm.doc.items2[i]["margin_type"],
							"margin_rate_or_amount": frm.doc.items2[i]["margin_rate_or_amount"],
							"rate_with_margin": frm.doc.items2[i]["rate_with_margin"],
							"discount_percentage": frm.doc.items2[i]["discount_percentage"],
							"discount_amount": frm.doc.items2[i]["discount_amount"],
							"base_rate_with_margin": frm.doc.items2[i]["base_rate_with_margin"],
							"rate": frm.doc.items2[i]["rate"],
							"amount": frm.doc.items2[i]["amount"],
							"item_tax_template": frm.doc.items2[i]["item_tax_template"],
							"base_rate": frm.doc.items2[i]["base_rate"],
							"base_amount": frm.doc.items2[i]["base_amount"],
							"pricing_rules": frm.doc.items2[i]["pricing_rules"],
							"stock_uom_rate": frm.doc.items2[i]["stock_uom_rate"],
							"is_free_item": frm.doc.items2[i]["is_free_item"],
							"grant_commission": frm.doc.items2[i]["grant_commission"],
							"net_rate": frm.doc.items2[i]["net_rate"],
							"net_amount": frm.doc.items2[i]["net_amount"],
							"base_net_rate": frm.doc.items2[i]["base_net_rate"],
							"base_net_amount": frm.doc.items2[i]["base_net_amount"],
							"billed_amt": frm.doc.items2[i]["billed_amt"],
							"valuation_rate": frm.doc.items2[i]["valuation_rate"],
							"gross_profit": frm.doc.items2[i]["gross_profit"],
							"drop_ship_section": frm.doc.items2[i]["drop_ship_section"],
							"delivered_by_supplier": frm.doc.items2[i]["delivered_by_supplier"],
							"supplier": frm.doc.items2[i]["supplier"],
							"item_weight_details": frm.doc.items2[i]["item_weight_details"],
							"weight_per_unit": frm.doc.items2[i]["weight_per_unit"],
							"total_weight": frm.doc.items2[i]["total_weight"],
							"weight_uom": frm.doc.items2[i]["weight_uom"],
							"warehouse_and_reference": frm.doc.items2[i]["warehouse_and_reference"],
							"warehouse": frm.doc.items2[i]["warehouse"],
							"target_warehouse": frm.doc.items2[i]["target_warehouse"],
							"prevdoc_docname": frm.doc.items2[i]["prevdoc_docname"],
							"against_blanket_order": frm.doc.items2[i]["against_blanket_order"],
							"blanket_order": frm.doc.items2[i]["blanket_order"],
							"blanket_order_rate": frm.doc.items2[i]["blanket_order_rate"],
							"manufacturing_section_section": frm.doc.items2[i]["manufacturing_section_section"],
							"bom_no": frm.doc.items2[i]["bom_no"],
							"planning_section": frm.doc.items2[i]["planning_section"],
							"projected_qty": frm.doc.items2[i]["projected_qty"],
							"actual_qty": frm.doc.items2[i]["actual_qty"],
							"ordered_qty": frm.doc.items2[i]["ordered_qty"],
							"planned_qty": frm.doc.items2[i]["planned_qty"],
							"work_order_qty": frm.doc.items2[i]["work_order_qty"],
							"produced_qty": frm.doc.items2[i]["produced_qty"],
							"delivered_qty": frm.doc.items2[i]["delivered_qty"],
							"returned_qty": frm.doc.items2[i]["returned_qty"],
							"shopping_cart_section": frm.doc.items2[i]["shopping_cart_section"],
							"additional_notes": frm.doc.items2[i]["additional_notes"],
							"page_break": frm.doc.items2[i]["page_break"],
							"item_tax_rate": frm.doc.items2[i]["item_tax_rate"],
							"transaction_date": frm.doc.items2[i]["transaction_date"]
						})
					}
					var tbl = cur_frm.get_field('items')
					tbl.grid.grid_rows[0].remove()



					cur_frm.refresh_fields()
				}
			])


		}, __('Create'));

		frm.set_query("item_code", 'items', function (doc) {
            if (frm.doc.item_group) {
                return {
                    query: "oburmod.queries.item_code_query",
                    filters: {
                        'item_group': frm.doc.item_group
                    }
                }
            }
            else {
                return { filters: {} }
            }
        });

		frm.set_query("item_code", "items2", function() {
			if (frm.doc.item_group) {
                return {
                    query: "oburmod.queries.item_code_query",
                    filters: {
                        'item_group': frm.doc.item_group
                    }
                }
            }
            else {
                return { filters: {} }
            }
		});
	},

	validate: function (frm) {
		frm.doc.total_total = frm.doc.total - frm.doc.total2
		frm.doc.total_quantity = frm.doc.total_qty - frm.doc.total_qty2
		frm.doc.items1n2 = ''
		frm.refresh_field('items1n2')
		for (let i in frm.doc.items)
						{
							frm.add_child('items1n2', {
								"item_code": frm.doc.items[i]["item_code"],
								"customer_item_code": frm.doc.items[i]["customer_item_code"],
								"ensure_delivery_based_on_produced_serial_no": frm.doc.items[i]["ensure_delivery_based_on_produced_serial_no"],
								"delivery_date": frm.doc.items[i]["delivery_date"],
								"item_name": frm.doc.items[i]["item_name"],
								"description": frm.doc.items[i]["description"],
								"item_group": frm.doc.items[i]["item_group"],
								"brand": frm.doc.items[i]["brand"],
								"image_section": frm.doc.items[i]["image_section"],
								"image": frm.doc.items[i]["image"],
								"image_view": frm.doc.items[i]["image_view"],
								"quantity_and_rate": frm.doc.items[i]["quantity_and_rate"],
								"qty": frm.doc.items[i]["qty"],
								"stock_uom": frm.doc.items[i]["stock_uom"],
								"picked_qty": frm.doc.items[i]["picked_qty"],
								"uom": frm.doc.items[i]["uom"],
								"conversion_factor": frm.doc.items[i]["conversion_factor"],
								"stock_qty": frm.doc.items[i]["stock_qty"],
								"price_list_rate": frm.doc.items[i]["price_list_rate"],
								"base_price_list_rate": frm.doc.items[i]["base_price_list_rate"],
								"discount_and_margin": frm.doc.items[i]["discount_and_margin"],
								"margin_type": frm.doc.items[i]["margin_type"],
								"margin_rate_or_amount": frm.doc.items[i]["margin_rate_or_amount"],
								"rate_with_margin": frm.doc.items[i]["rate_with_margin"],
								"discount_percentage": frm.doc.items[i]["discount_percentage"],
								"discount_amount": frm.doc.items[i]["discount_amount"],
								"base_rate_with_margin": frm.doc.items[i]["base_rate_with_margin"],
								"rate": frm.doc.items[i]["rate"],
								"amount": frm.doc.items[i]["amount"],
								"item_tax_template": frm.doc.items[i]["item_tax_template"],
								"base_rate": frm.doc.items[i]["base_rate"],
								"base_amount": frm.doc.items[i]["base_amount"],
								"pricing_rules": frm.doc.items[i]["pricing_rules"],
								"stock_uom_rate": frm.doc.items[i]["stock_uom_rate"],
								"is_free_item": frm.doc.items[i]["is_free_item"],
								"grant_commission": frm.doc.items[i]["grant_commission"],
								"net_rate": frm.doc.items[i]["net_rate"],
								"net_amount": frm.doc.items[i]["net_amount"],
								"base_net_rate": frm.doc.items[i]["base_net_rate"],
								"base_net_amount": frm.doc.items[i]["base_net_amount"],
								"billed_amt": frm.doc.items[i]["billed_amt"],
								"valuation_rate": frm.doc.items[i]["valuation_rate"],
								"gross_profit": frm.doc.items[i]["gross_profit"],
								"drop_ship_section": frm.doc.items[i]["drop_ship_section"],
								"delivered_by_supplier": frm.doc.items[i]["delivered_by_supplier"],
								"supplier": frm.doc.items[i]["supplier"],
								"item_weight_details": frm.doc.items[i]["item_weight_details"],
								"weight_per_unit": frm.doc.items[i]["weight_per_unit"],
								"total_weight": frm.doc.items[i]["total_weight"],
								"weight_uom": frm.doc.items[i]["weight_uom"],
								"warehouse_and_reference": frm.doc.items[i]["warehouse_and_reference"],
								"warehouse": frm.doc.items[i]["warehouse"],
								"target_warehouse": frm.doc.items[i]["target_warehouse"],
								"prevdoc_docname": frm.doc.items[i]["prevdoc_docname"],
								"against_blanket_order": frm.doc.items[i]["against_blanket_order"],
								"blanket_order": frm.doc.items[i]["blanket_order"],
								"blanket_order_rate": frm.doc.items[i]["blanket_order_rate"],
								"manufacturing_section_section": frm.doc.items[i]["manufacturing_section_section"],
								"bom_no": frm.doc.items[i]["bom_no"],
								"planning_section": frm.doc.items[i]["planning_section"],
								"projected_qty": frm.doc.items[i]["projected_qty"],
								"actual_qty": frm.doc.items[i]["actual_qty"],
								"ordered_qty": frm.doc.items[i]["ordered_qty"],
								"planned_qty": frm.doc.items[i]["planned_qty"],
								"work_order_qty": frm.doc.items[i]["work_order_qty"],
								"produced_qty": frm.doc.items[i]["produced_qty"],
								"delivered_qty": frm.doc.items[i]["delivered_qty"],
								"returned_qty": frm.doc.items[i]["returned_qty"],
								"shopping_cart_section": frm.doc.items[i]["shopping_cart_section"],
								"additional_notes": frm.doc.items[i]["additional_notes"],
								"page_break": frm.doc.items[i]["page_break"],
								"item_tax_rate": frm.doc.items[i]["item_tax_rate"],
								"transaction_date": frm.doc.items[i]["transaction_date"]
							})
					}
					for (let i in frm.doc.items2)
					{
						frm.add_child('items1n2', {
							"item_code": frm.doc.items2[i]["item_code"],
							"customer_item_code": frm.doc.items2[i]["customer_item_code"],
							"ensure_delivery_based_on_produced_serial_no": frm.doc.items2[i]["ensure_delivery_based_on_produced_serial_no"],
							"delivery_date": frm.doc.items2[i]["delivery_date"],
							"item_name": frm.doc.items2[i]["item_name"],
							"description": frm.doc.items2[i]["description"],
							"item_group": frm.doc.items2[i]["item_group"],
							"brand": frm.doc.items2[i]["brand"],
							"image_section": frm.doc.items2[i]["image_section"],
							"image": frm.doc.items2[i]["image"],
							"image_view": frm.doc.items2[i]["image_view"],
							"quantity_and_rate": frm.doc.items2[i]["quantity_and_rate"],
							"qty": -frm.doc.items2[i]["qty"],
							"stock_uom": frm.doc.items2[i]["stock_uom"],
							"picked_qty": frm.doc.items2[i]["picked_qty"],
							"uom": frm.doc.items2[i]["uom"],
							"conversion_factor": frm.doc.items2[i]["conversion_factor"],
							"stock_qty": frm.doc.items2[i]["stock_qty"],
							"price_list_rate": frm.doc.items2[i]["price_list_rate"],
							"base_price_list_rate": frm.doc.items2[i]["base_price_list_rate"],
							"discount_and_margin": frm.doc.items2[i]["discount_and_margin"],
							"margin_type": frm.doc.items2[i]["margin_type"],
							"margin_rate_or_amount": frm.doc.items2[i]["margin_rate_or_amount"],
							"rate_with_margin": frm.doc.items2[i]["rate_with_margin"],
							"discount_percentage": frm.doc.items2[i]["discount_percentage"],
							"discount_amount": frm.doc.items2[i]["discount_amount"],
							"base_rate_with_margin": frm.doc.items2[i]["base_rate_with_margin"],
							"rate": frm.doc.items2[i]["rate"],
							"amount": frm.doc.items2[i]["amount"],
							"item_tax_template": frm.doc.items2[i]["item_tax_template"],
							"base_rate": frm.doc.items2[i]["base_rate"],
							"base_amount": frm.doc.items2[i]["base_amount"],
							"pricing_rules": frm.doc.items2[i]["pricing_rules"],
							"stock_uom_rate": frm.doc.items2[i]["stock_uom_rate"],
							"is_free_item": frm.doc.items2[i]["is_free_item"],
							"grant_commission": frm.doc.items2[i]["grant_commission"],
							"net_rate": frm.doc.items2[i]["net_rate"],
							"net_amount": frm.doc.items2[i]["net_amount"],
							"base_net_rate": frm.doc.items2[i]["base_net_rate"],
							"base_net_amount": frm.doc.items2[i]["base_net_amount"],
							"billed_amt": frm.doc.items2[i]["billed_amt"],
							"valuation_rate": frm.doc.items2[i]["valuation_rate"],
							"gross_profit": frm.doc.items2[i]["gross_profit"],
							"drop_ship_section": frm.doc.items2[i]["drop_ship_section"],
							"delivered_by_supplier": frm.doc.items2[i]["delivered_by_supplier"],
							"supplier": frm.doc.items2[i]["supplier"],
							"item_weight_details": frm.doc.items2[i]["item_weight_details"],
							"weight_per_unit": frm.doc.items2[i]["weight_per_unit"],
							"total_weight": frm.doc.items2[i]["total_weight"],
							"weight_uom": frm.doc.items2[i]["weight_uom"],
							"warehouse_and_reference": frm.doc.items2[i]["warehouse_and_reference"],
							"warehouse": frm.doc.items2[i]["warehouse"],
							"target_warehouse": frm.doc.items2[i]["target_warehouse"],
							"prevdoc_docname": frm.doc.items2[i]["prevdoc_docname"],
							"against_blanket_order": frm.doc.items2[i]["against_blanket_order"],
							"blanket_order": frm.doc.items2[i]["blanket_order"],
							"blanket_order_rate": frm.doc.items2[i]["blanket_order_rate"],
							"manufacturing_section_section": frm.doc.items2[i]["manufacturing_section_section"],
							"bom_no": frm.doc.items2[i]["bom_no"],
							"planning_section": frm.doc.items2[i]["planning_section"],
							"projected_qty": frm.doc.items2[i]["projected_qty"],
							"actual_qty": frm.doc.items2[i]["actual_qty"],
							"ordered_qty": frm.doc.items2[i]["ordered_qty"],
							"planned_qty": frm.doc.items2[i]["planned_qty"],
							"work_order_qty": frm.doc.items2[i]["work_order_qty"],
							"produced_qty": frm.doc.items2[i]["produced_qty"],
							"delivered_qty": frm.doc.items2[i]["delivered_qty"],
							"returned_qty": frm.doc.items2[i]["returned_qty"],
							"shopping_cart_section": frm.doc.items2[i]["shopping_cart_section"],
							"additional_notes": frm.doc.items2[i]["additional_notes"],
							"page_break": frm.doc.items2[i]["page_break"],
							"item_tax_rate": frm.doc.items2[i]["item_tax_rate"],
							"transaction_date": frm.doc.items2[i]["transaction_date"]
						})
					}
		// frm.refresh_field('items1n2')
					// var tbl = cur_frm.get_field('items')
					// tbl.grid.grid_rows[0].remove()

	}



})


frappe.ui.form.on('Sales Order Item', {
	item_code: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn]
		var t = ['item_name', 'item_group', 'description']
		msgprint(r+'')
		// for (let i in t)
		// frm.call({
		// 	doc: frm.doc,
		// 	method: 'get_value',
		// 	args: {
		// 		docname: d.item_code,
		// 		field: t[i]
		// 	},
		// 	callback: function (r) {
		// 		d[t[i]] = r.message
		// 	}
		// })
		// frm.refresh_fields()
	},
	qty: function (frm, cdt, cdn) {
		set_total1(frm, cdt, cdn)

		// To set Quantity
		// total_qty
		let t = 0
		for (let i in frm.doc.items) {
			t += frm.doc.items[i].qty
		}
		frm.doc.total_qty = t
		frm.refresh_field('total_qty')
		
	},
	rate: function (frm, cdt, cdn) {
		set_total1(frm, cdt, cdn)
	}
			
})

frappe.ui.form.on('Sales Order Item2', {
	qty: function (frm, cdt, cdn) {
		set_total2(frm, cdt, cdn)

		// To set Quantity
		// total_qty
		let t = 0
		for (let i in frm.doc.items2) {
			t += frm.doc.items2[i].qty
		}
		frm.doc.total_qty2 = t
		frm.refresh_field('total_qty2')
		
	},
	rate: function (frm, cdt, cdn) {
		set_total2(frm, cdt, cdn)
	}
			
})

function set_total1(frm, cdt, cdn) {
	var d = locals[cdt][cdn]
		d.amount = d.qty * d.rate
		frm.refresh_field('amount')
		frm.refresh_field('items')

		let t = 0
		for (let i in frm.doc.items) {
			t += frm.doc.items[i].amount
		}
		frm.doc.total = t
		frm.refresh_field('total')
}

function set_total2(frm, cdt, cdn) {
	var d = locals[cdt][cdn]
		d.amount = d.qty * d.rate
		frm.refresh_field('amount')
		frm.refresh_field('items2')

		let t = 0
		for (let i in frm.doc.items2) {
			t += frm.doc.items2[i].amount
		}
		frm.doc.total2 = t
		frm.refresh_field('total2')
}

// function make_sales_invoice() {
// 	frappe.model.open_mapped_doc({
// 		method: "erpnext.selling.doctype.sales_order.sales_order.make_sales_invoice",
// 		// frm: frm
// 	})
// }