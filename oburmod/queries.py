from __future__ import unicode_literals


import frappe
# import erpnext

from frappe.desk.reportview import get_match_cond, get_filters_cond
from collections import defaultdict


@frappe.whitelist()
def item_code_query(doctype, txt, searchfield, start, page_len, filters):
    parent_group = filters.get('item_group', False)
    if parent_group:
        query = f"""SELECT item_code, item_group
        FROM tabItem
        WHERE item_group IN (
            SELECT name
            FROM `tabItem Group`
            WHERE parent_item_group = '{parent_group}'
            );"""
    return frappe.db.sql(query)


@frappe.whitelist()
def stock_entry_type_query(doctype, txt, searchfield, start, page_len, filters):
    query = f"""SELECT name
    FROM `tabStock Entry Type`
    WHERE name in ('Material Issue', 'Material Transfer', 'Material Receipt');
    
    """
    return frappe.db.sql(query)
